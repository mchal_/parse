# Pa-rs E
A rusty password manager that'll blow your arse off.

## The What
Pa-rs E is a simple, Rust-based password manager that implements an equally memorable and secure password format. All passwords are created using only 3 components:

- A randomly generated 6-8 character string
- A phrase the user keeps memorized
- A 127-character ASCII cipher to "cipher" service names

## The Why
Through combining these, the user can easily generate memorable passwords that are still lengthy, unique and secure. Since the memorable phrase is required to generate the password, it is not stored anywhere. The user is responsible for keeping it in their head. This allows the rest of the password to:

- Be unique to each service
- Never wholly be stored anywhere
- Be easily generated

And allows the cipher and leading string to be stored on the machine in plaintext, while not compromising on security.

## Usage
Pa-rs E works using multiple profiles. Each profile has a unique cipher and a leading string.
The user can switch between profiles using the `--profile`/`-p` flag.

| Subcommand | Description |
| ---------- | ----------- |
| `profile` `new/delete` | Initializes or deletes a `Pa-rs E` profile |
| `parse` `-p (profile)` | Parses a new password from the user-provided service name, if a profile is not specified, "default" is used |

## Installation

### From Source
1. Clone the repository
```bash
git clone https://gitlab.com/mchal_/parse
```

2. Install using Cargo
```bash
cargo install --path .
```

### Nix

#### Flakes
```bash
nix profile install gitlab:mchal_/parse
```

#### Nixpkgs (Not yet available)
```nix
environment.systemPackages = with pkgs; [
  pa-rs-e
];
```

### Arch Linux

#### From Source
1. Clone and enter the repository
```bash
git clone https://gitlab.com/mchal_/parse
cd parse
```

2. Run `makepkg` to build and install the package
```bash
makepkg -si
```

#### AUR
```bash
yay/paru/ame -S pa-rs-e
```