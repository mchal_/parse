#![feature(arbitrary_enum_discriminant)]
#![doc = include_str!("../README.md")]

use args::ProfileCommands;
use clap::Parser;
use colored::Colorize;

pub mod args;
use args::Args;
use args::Operation;
use args::ParseArgs;

pub mod config;
use config::Config;
use config::Generated;
use config::Profile;

pub mod error;
pub use error::AppError;

pub mod utils;

fn main() {
    let args: Args = Args::parse();

    let profile = args.profile;

    let result = match args.subcommand {
        Operation::Profile(profile_commands) => profile_cmd(profile_commands),
        Operation::Parse(parse_args) => parse_cmd(parse_args, profile),
    };

    if let Err(e) = result {
        crash!(e, "{}", e);
    }
}

fn profile_cmd(commands: ProfileCommands) -> Result<(), AppError> {
    match commands {
        ProfileCommands::New { profile } => {
            info!("Initializing new profile \"{}\"...", profile);
            let mut config = Config::load()?;

            config.add(&profile, Profile::new())?;
            config.save()?;

            success!("Successfully created profile \"{}\"", profile);
        }
        ProfileCommands::Delete { profile } => {
            info!("Removing profile \"{}\"...", profile);
            let mut config = Config::load()?;

            config.delete(&profile)?;
            config.save()?;

            success!("Successfully deleted profile \"{}\"", profile);
        }
    }

    Ok(())
}

fn parse_cmd(args: ParseArgs, profile: String) -> Result<(), AppError> {
    info!("Parsing password(s)...");

    let config = Config::load()?;
    let profile_loaded = config
        .profile
        .get(&profile)
        .ok_or_else(|| AppError::ProfileError(format!("Profile \"{}\" does not exist", profile)))?;
    let mut generated: Vec<Generated> = vec![];

    for service in &args.services {
        let ciphered = config.password(service.to_string(), profile.clone())?;
        generated.push(ciphered);
    }

    if generated.len() > 1 {
        success!("Your new passwords are:");
    } else {
        success!("Your new password is:");
    }

    for password in generated {
        println!(
            "   {}:\n   {} + Your Phrase + {}",
            password.service.bold(),
            profile_loaded.lead,
            password.ciphered
        );
    }

    Ok(())
}
