//! # Error
//! This module defines the error type used by the application ([`AppError`])
//!
//! It also contains the [`AppError::code`] function which returns the exit code
//! for the error, as well as multilpe From implementations for the error type.

use std::fmt::{Display, Formatter};

/// An Enum representing various errors that the program can encounter
#[allow(clippy::enum_variant_names)]
#[derive(Debug)]
#[repr(i32)]
pub enum AppError {
    /// A wrapper around [`std::io::Error`], used for errors that occur while
    /// reading or writing to files
    IoError(std::io::Error) = 3,

    /// A wrapper around [`toml::de::Error`], used for errors that occur while
    /// deserializing [`crate::config::Config`] to TOML
    DeserError(toml::de::Error) = 4,

    /// A wrapper around [`toml::ser::Error`], used for errors that occur while
    /// serializing [`crate::config::Config`] to TOML
    SerError(toml::ser::Error) = 5,

    /// Any error that can occur when operating on a profile
    ProfileError(String) = 6,

    /// A general error, non-specific to any other error type
    /// This is used for anything that doesn't fit into the above error types
    GeneralError(String) = 1,
}

impl AppError {
    /// Returns the exit code assigned to the error
    pub fn code(&self) -> i32 {
        unsafe { *(self as *const Self as *const i32) }
    }
}

impl From<std::io::Error> for AppError {
    fn from(err: std::io::Error) -> Self {
        AppError::IoError(err)
    }
}

impl From<toml::de::Error> for AppError {
    fn from(err: toml::de::Error) -> Self {
        AppError::DeserError(err)
    }
}

impl From<toml::ser::Error> for AppError {
    fn from(err: toml::ser::Error) -> Self {
        AppError::SerError(err)
    }
}

impl Display for AppError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            AppError::IoError(err) => write!(f, "IO Error: {}", err),
            AppError::DeserError(err) => write!(f, "Deserialization Error: {}", err),
            AppError::SerError(err) => write!(f, "Serialization Error: {}", err),
            AppError::ProfileError(err) => write!(f, "Profile Error: {}", err),
            AppError::GeneralError(err) => write!(f, "General Error: {}", err),
        }
    }
}
