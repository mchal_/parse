//! # Args
//!
//! This module contains the argument parsing logic for the application.

use clap::{Parser, Subcommand};

#[derive(Debug, Clone, Parser)]
#[clap(name = env!("CARGO_PKG_NAME"), version = env!("CARGO_PKG_VERSION"), author = env!("CARGO_PKG_AUTHORS"), about = env!("CARGO_PKG_DESCRIPTION"), infer_subcommands = true)]
pub struct Args {
    #[clap(subcommand)]
    pub subcommand: Operation,

    /// The name of the profile to create (default: "default")
    #[clap(short, long, default_value = "default", global = true)]
    pub profile: String,
}

#[derive(Debug, Clone, Subcommand)]
pub enum Operation {
    /// Operate on a "Pa-rs E" profile
    #[clap(subcommand)]
    Profile(ProfileCommands),

    /// Parse passwords using a generated config file
    #[clap(name = "parse")]
    Parse(ParseArgs),
}

#[derive(Debug, Clone, Subcommand)]
pub enum ProfileCommands {
    /// Initialize a new "Pa-rs E" profile
    #[clap(name = "new")]
    New {
        /// The name of the profile to create (default: "default")
        #[clap(default_value = "default")]
        profile: String,
    },

    /// Delete an existing "Pa-rs E" profile
    #[clap(name = "delete")]
    Delete {
        /// The name of the profile to delete
        #[clap(required = true)]
        profile: String,
    },
}

#[derive(Debug, Clone, Parser)]
pub struct ParseArgs {
    /// The name of the service(s) the password is for
    /// (e.g. "gmail", "facebook", "reddit", etc.)
    #[clap(name = "service", required = true)]
    pub services: Vec<String>,
}
