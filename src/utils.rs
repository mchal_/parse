//! # Utils
//! This module contains some utility functions that are used in the rest of the
//! program, notably formatted print strings

use colored::Colorize;

use crate::AppError;

const LOGO: &str = "🦀";
#[allow(dead_code)]
const WARN: &str = "⚠️";
const BURN: &str = "🔥";
const SUCC: &str = "✅";

#[macro_export]
macro_rules! info {
    ($($arg:tt)*) => {
        $crate::utils::info_fn(format!($($arg)*))
    }
}

#[macro_export]
macro_rules! warn {
    ($($arg:tt)*) => {
        $crate::utils::warn_fn(format!($($arg)*))
    }
}

#[macro_export]
macro_rules! crash {
    ($error:expr, $($arg:tt)*) => {
        $crate::utils::crash_fn(format!($($arg)*), $error)
    }
}

#[macro_export]
macro_rules! success {
    ($($arg:tt)*) => {
        $crate::utils::success_fn(format!($($arg)*))
    }
}

pub fn info_fn(msg: String) {
    println!("{} {}", LOGO, msg.bold());
}

#[allow(dead_code)]
pub fn warn_fn(msg: String) {
    println!("{} {}", WARN, msg.yellow().bold());
}

pub fn crash_fn(msg: String, error: AppError) {
    println!("{} {}", BURN, msg.red().bold());
    std::process::exit(error.code());
}

pub fn success_fn(msg: String) {
    println!("{} {}", SUCC.green(), msg.bold());
}
