//! # Config
//!
//! This module contains multiple structs and functions for reading and writing to
//! and from the config.
//!
//! It includes the structs for:
//!
//! - [`Config`]: The main config struct
//! - [`Profile`]: The profile struct
//! - [`Generated`]: The generated password struct

use std::collections::hash_map::Entry::Vacant;
use std::collections::HashMap;
use std::fs::{File, OpenOptions};
use std::io::{Read, Write};

use rand::{distributions::Alphanumeric, thread_rng, Rng};
use serde::{Deserialize, Serialize};

use crate::AppError;

pub struct Generated {
    pub config: Config,
    pub service: String,
    pub ciphered: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
/// A struct representing a Pa-rs E config
pub struct Config {
    pub profile: HashMap<String, Profile>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Profile {
    /// The 6..8 character leading string
    pub lead: String,

    /// The 127 length cipher to map service names agains
    pub cipher: String,
}

impl Default for Profile {
    fn default() -> Self {
        Self::new()
    }
}

impl Profile {
    /// Generates a new profile with a random [`Profile::lead`] and [`Profile::cipher`]
    pub fn new() -> Self {
        let dist = Alphanumeric;

        let int = thread_rng().gen_range(6..=8);
        let lead = (0..int)
            .map(|_| thread_rng().sample(dist) as char)
            .collect::<String>();

        let cipher = (0..=127)
            .map(|_| thread_rng().sample(dist) as char)
            .collect::<String>();

        Self { lead, cipher }
    }
}

impl Default for Config {
    fn default() -> Self {
        Self::new()
    }
}

impl Config {
    /// Generates a new empty [`Config`]
    pub fn new() -> Self {
        Self {
            profile: HashMap::new(),
        }
    }

    /// Attempts to save a provided [`Config`] to the default path (OS dependent)
    ///
    /// # Errors
    ///
    /// * [`AppError::IoError`] - If there is an error writing to the file
    /// * [`AppError::SerError`] - If there is an error serializing the given [`Config`] to TOML
    /// * [`AppError::GeneralError`] - If the default path cannot be found or is not accessible
    pub fn save(&self) -> Result<(), AppError> {
        let config = toml::to_string_pretty(self)?;

        let path = Config::path()?;
        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(path)?;

        file.set_len(0)?;
        file.write_all(config.as_bytes())?;

        Ok(())
    }

    /// Attempts to load a [`Config`] from the default path (OS dependent)
    ///
    /// # Errors
    ///
    /// * [`AppError::IoError`] - If there is an error reading from the file
    /// * [`AppError::DeserError`] - If there is an error deserializing the TOML to a [`Config`]
    /// * [`AppError::GeneralError`] - If the default path cannot be found or is not accessible
    pub fn load() -> Result<Self, AppError> {
        if !Config::path()?.exists() {
            return Ok(Config::new());
        }

        let mut config = String::new();

        let mut file = File::open(Config::path()?)?;
        file.read_to_string(&mut config)?;

        let config = toml::from_str(&config)?;

        Ok(config)
    }

    /// Adds a new [`Profile`] to the [`Config`]
    pub fn add<S: ToString>(&mut self, name: S, profile: Profile) -> Result<Config, AppError> {
        let name = name.to_string();

        if let Vacant(e) = self.profile.entry(name.clone()) {
            e.insert(profile);

            Ok(self.to_owned())
        } else {
            Err(AppError::ProfileError(format!(
                "Profile \"{}\" already exists",
                name
            )))
        }
    }

    /// Removes a [`Profile`] from the [`Config`]
    pub fn delete<S: ToString>(&mut self, profile: S) -> Result<Config, AppError> {
        let profile = profile.to_string();

        if self.profile.contains_key(&profile) {
            self.profile.remove(&profile);

            Ok(self.to_owned())
        } else {
            Err(AppError::ProfileError(format!(
                "Profile \"{}\" does not exist",
                profile
            )))
        }
    }

    /// Returns the path to the config file
    ///
    /// # Errors
    ///
    /// * [`AppError::GeneralError`] - If the path cannot be found or is not accessible
    pub fn path() -> Result<std::path::PathBuf, AppError> {
        if let Some(dir) = dirs::config_dir() {
            Ok(dir.join("pa-rs-e.toml"))
        } else {
            Err(AppError::GeneralError(
                "Could not find config directory".to_string(),
            ))
        }
    }

    /// Returns a tuple of the [`Config`], the ciphered service and the provided service
    ///
    /// # Arguments
    ///
    /// * `service` - The service to get the password for
    pub fn password(&self, service: String, profile: String) -> Result<Generated, AppError> {
        let mut ciphered = String::new();
        if let Some(profile) = self.profile.get(&profile) {
            for c in service.chars() {
                let index = c as u8;
                let ciphered_char = profile.cipher.as_bytes()[index as usize] as char;

                ciphered.push(ciphered_char);
            }

            Ok(Generated {
                config: self.clone(),
                service,
                ciphered,
            })
        } else {
            Err(AppError::ProfileError(format!(
                "Could not find profile \"{}\"",
                profile
            )))
        }
    }
}
